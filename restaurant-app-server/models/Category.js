const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = mongoose.Schema({
	name: String
});

module.exports = mongoose.model('Category', CategorySchema);
