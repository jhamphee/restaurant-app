const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ItemSchema = new Schema({
	name: String,
	price: Number,
    image: String,
	category: String
});

module.exports = mongoose.model('Item', ItemSchema);
