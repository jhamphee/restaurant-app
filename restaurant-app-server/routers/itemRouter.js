const express = require('express');
const path = require('path');
const fs = require('fs');
// const multer = require('multer');

const Item = require('../models/Item');

const router = express.Router();


router.get('/', (req, res) => {
	Item.find({}).populate("item")
	.then(data => {
		res.send(data)
	});	
})

// router.get('/:id', (req, res) => {
// 	Item.findById(req.params.id).populate("category")
// 	.then(data => {
// 		res.send(data)
// 	});	
// })

router.post('/', (req, res) => {
    let newItem = new Item (req.body);
    newItem.save()
    .then(data => {
        res.send(data);
    });
})

router.delete('/:id', (req, res) => {
	Item.findOneAndDelete({_id: req.params.id}, { useFindAndModify: false }) 
	.then(data => {
		// fs.unlinkSync("./public/uploads/"+data.image);
		res.send(data);
	});
})

// router.put("/:id", upload.single('myImage'), (req, res) => {
// 	let item = req.body;
// 	if(req.file) item.image = req.file.filename;
// 	Item.findOneAndUpdate({_id: req.params.id}, req.body, { new: true, useFindAndModify: false })
// 	.populate("category")
// 	.then(data => {
// 		res.send(data);
// 	})
// })

module.exports = router;
