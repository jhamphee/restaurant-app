import React from 'react';
import { connect } from 'react-redux';

const axios = require('axios')

const dataBaseItemHandler = (data) => {
    return {
      type: "ITEM_DATA",
      payload: data
    }
 }

const Items = (props) => {

    componentDidMount(){
        axios.get('http://localhost:8080/item')
        .then(res => {
          let newItem = res.data;
    
          this.props.addToState(newItem)
        });
      }

      itemDisplayHandler = () => {

      }
      
    return(
        <div className = "contentBodyContainer">

        </div>
    )
}

const mapStateToProps = (state) => {
    return {item : state.item}
}

const mapDispatchToProps = (dispatch) => {
    return {
      addToState : (newItem) => {dispatch(dataBaseItemHandler(newItem))}
  
    }
  }

export default (connect)(mapStateToProps, mapDispatchToProps)(Items);