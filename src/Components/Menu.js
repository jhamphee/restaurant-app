import React from 'react';
import { connect } from 'react-redux';

const axios = require('axios')

const Menu = (props) => {

    const itemCategoryHandler = () => {
        
    }

    const menuDisplayHandler = () => {
        let menuDisplay = props.category.map(a => {
            return <div className = "menuCategoryDisplay" onClick = {itemCategoryHandler}>
                        {a.name}
                   </div>
        })
        return menuDisplay
    }



    return(
        <div className = "contentBodyContainer clearfix">
            {menuDisplayHandler()}
        </div>
    )
}


const mapStateToProps = (state) => {
    return {category : state.category}
}

export default connect(mapStateToProps)(Menu);
